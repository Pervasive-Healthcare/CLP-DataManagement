# CLP - Data Management

The **Data Management** component provides to standardize heterogeneous datasets in a single homogeneous dataset.

[<img src="/data-management-arch__1_.png" width="550"/>](/data-management-arch__1_.png)

The component includes the following modules:
- **Data aligner**: standardizes the data received at the input using a given frequency and for each type of inertial data the relative unit of measurement according to a specific configuration.
- **Feature extractor**: allows you to calculate the magnitude of the signal and a set of features that analyze its characteristics, after the signals have been standardized both in frequency and in units of measurement.
- **Label comparator**: provides support to users in the label standardization phase. As mentioned in the previous section, it is necessary to group under the same common label all the data series that identifies the same activity not based solely on the syntactic equality of the label.

The Data Management component communicates, through the RabbitMQ message broker, with the Repository Manager component, both for sending the output data and for receiving the time series to be processed as input. Furthermore, it also indirectly communicates with the Web Application, with which the user will see suggestions regarding the coupling of the labels.

## APIs


- **/uniform/\<dbName\> [POST]** : allows to map the activities of the dataset \<dbName\> to the activity of the uniformed dataset. Starts the uniformation process. Body example:

        {
            "standing": "standing",
            "walking": "walking",
            "jogging": "jogging",
            "jumping": "jumping",
            "walking upstairs": "stairs up",
            "walking downstairs": "stairs down"
        }


- **/config [POST]** : allows to add a new configuration in the platform. Body example:

        {  
            "type": "new_config",  
            "frequency": 50,
            "uom": {
                "accelerometer": "m/s^2",
                "gyroscope": "rad/s"
            }
        }

