import json
import threading

import pika
import yaml
from flask import Flask, request
from flask_cors import CORS

from Core.RabbitMQ.message_sender import send_message
from Core.RabbitMQ.rabbitmq_manager import init_rabbitmq
from Utils.enums import Config
from Utils.json_validator import validate_json, JsonSchema

with open("config.yml", "r") as yml_file:
    config = yaml.safe_load(yml_file)

rabbitmq_config = config[Config.RABBIT_MQ_CONNECTION.value]
app = Flask(__name__)
CORS(app)


#######################################################################################################################
#                                           /uniform/<db_name>
#######################################################################################################################
@app.route("/uniform/<db_name>", methods=["POST"])
def trigger_uniform(db_name):
    if request.method == "POST":

        credentials = pika.PlainCredentials(rabbitmq_config.get('user'), rabbitmq_config.get('password'))
        parameters = pika.ConnectionParameters(rabbitmq_config.get('host'),
                                               rabbitmq_config.get('port'),
                                               '/',
                                               heartbeat=1800,
                                               credentials=credentials)
        

        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        #channel.basic_qos(prefetch_count=10)

        new_labels = request.get_json()
        new_labels['type'] = 'uniform_label'
        new_labels['db_id'] = db_name
        channel.basic_publish(exchange='', routing_key='req_to_repo', body=json.dumps(new_labels))
        connection.close()
        print("--> Send uniform request --> %s" % new_labels)
        return {"uniform": db_name}


#######################################################################################################################
#                                               /config
#######################################################################################################################
@app.route("/config", methods=["POST"])
def trigger_add_new_config():
    if request.method == "POST":
        config_req = request.get_json()
        print(config_req)
        if not validate_json(config_req, JsonSchema.CONFIG_SCHEMA, "new config"):
            return {"new_config_added": False}
        threading.Thread(target=perform_action_new_config, args=[request.json]).start()
        return {"new_config_added": True}


def perform_action_new_config(post_body):
    credentials = pika.PlainCredentials(rabbitmq_config.get('user'), rabbitmq_config.get('password'))
    parameters = pika.ConnectionParameters(rabbitmq_config.get('host'),
                                           rabbitmq_config.get('port'),
                                           '/',
                                           heartbeat=1800,
                                           credentials=credentials)
    


    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    #channel.basic_qos(prefetch_count=10)

    new_dbinfo = {
        "type": "new_config",
    }
    new_dbinfo.update(post_body)

    channel.basic_publish(exchange='', routing_key='req_to_repo', body=json.dumps(new_dbinfo))
    connection.close()
    print("New config sent")


def flask_thread():
    app.run(host="0.0.0.0", port=5002)


#######################################################################################################################
#                                               MAIN
#######################################################################################################################
if __name__ == "__main__":
    thread = threading.Thread(target=flask_thread)
    thread.start()

    init_rabbitmq(config[Config.RABBIT_MQ_CONNECTION.value])
