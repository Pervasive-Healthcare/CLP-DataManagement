import json
import pika

from Core.RabbitMQ.message_sender import init_channel
from Core.manager.importer_manager import on_end_importing_default_response_received, on_imported_data_received, \
    on_end_receiving_imported_data
from Core.manager.uniformer_manager import on_end_to_uniform_labels, on_imported_labeled_data_received


#######################################################################################################################
#                                           RABBITMQ CALLBACKS
#######################################################################################################################

def data_to_dm_callback(ch, method, properties, body):
    data = json.loads(body)
    data_type = data['type']
    if data_type == "to_uniform_default":
        on_imported_data_received(data)
    elif data_type == "to_uniform":
        on_imported_labeled_data_received(data)
    elif data_type == "end_importing_default":
        on_end_importing_default_response_received(data)
    elif data_type == "end_to_send_imported_data":
        on_end_receiving_imported_data(data)


def response_callback(ch, method, properties, body):
    response = json.loads(body)
    response_type = response['type']
    if response_type == 'end_to_uniform_labels':
        on_end_to_uniform_labels(response)


def init_rabbitmq(rabbitmq_config):
    global channel
    credentials = pika.PlainCredentials(rabbitmq_config.get('user'), rabbitmq_config.get('password'))
    parameters = pika.ConnectionParameters(rabbitmq_config.get('host'),
                                           rabbitmq_config.get('port'),
                                           '/',
                                           heartbeat=1800,
                                           credentials=credentials)
    


    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    

    channel.queue_declare(queue='data_to_dm')
    channel.queue_declare(queue='res_to_dm')
    
    #channel.basic_qos(prefetch_count=1)

    channel.basic_consume(queue='data_to_dm', on_message_callback=data_to_dm_callback, auto_ack=True)
    #channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='res_to_dm', on_message_callback=response_callback, auto_ack=True)

    init_channel(channel)

    channel.start_consuming()

