import matlab.engine
import numpy as np

print("Starting matlab engine...")
eng = matlab.engine.start_matlab()
eng.cd('Core/MatLab')
print("Matlab engine started")


WINDOW_SIZE = 150
WINDOW_OVERLAY = 50
WINDOW_START = WINDOW_SIZE - WINDOW_OVERLAY


def compute_mean(matrix):
    x = np.array(matrix)
    return np.mean(x, axis=0)


def convert_to_matlab_array(windows):
    sample_window_matlab_matrix = matlab.double(size=[len(windows), WINDOW_SIZE])
    # per ogni window creo la matrice da dare in input a matlab
    for i, window in enumerate(windows):
        for j in range(0, len(window)):
            sample_window_matlab_matrix[i][j] = window[j]
    return sample_window_matlab_matrix


def extract_feature(array):

    size = len(array)
    sample_windows = []

    # creo le windows di dati (sample_windows)
    for i in range(0, int(size / WINDOW_START)):
        start = i * WINDOW_START
        end = i * WINDOW_START + WINDOW_SIZE
        sample_windows.append(array[start: end])

    # Converto la finestra in un formato utilizzabile da matlab
    sample_window_matlab_matrix = convert_to_matlab_array(sample_windows)

    # Estraggo le feature dalla window corrente
    single_sample_feature = eng.fe(sample_window_matlab_matrix)

    # Media delle feature
    feature_mean = compute_mean(single_sample_feature)

    return feature_mean




'''

def extract_features_from_imported_data(db_id):
    data, dbinfo = get_all_data_by_id2(db_id)
    final_features = {}

    activities = dbinfo.get('activities')

    for activity in activities:
        activity_data = [sample for sample in data if sample.get('label') == activity]
        features_current_activity = []

        for sample in activity_data:
            acc_data = sample.get('accelerometer')
            sample_windows = []

            size = len(acc_data)
            print("------")

            # creo le windows di dati (sample_windows)
            for i in range(0, int(size / WINDOW_START)):
                start = i * WINDOW_START
                end = i * WINDOW_START + WINDOW_SIZE
                sample_windows.append(acc_data[start: end])

            # Converto le finestre in un formato utilizzabile da matlab
            sample_window_matlab_matrix = convert_to_matlab_array(sample_windows)

            # Estraggo le feature dalla window corrente
            single_sample_feature = eng.fe(sample_window_matlab_matrix)

            # Media delle feature
            feature_mean = compute_mean(single_sample_feature)

            # Aggiungo la media a features_current_activity
            features_current_activity.append(feature_mean)

        # Caloclo la media di tutti i dati di una data activity
        final_features[activity] = compute_mean(features_current_activity).tolist()

    return final_features
'''




