function [dataout] = fe(datain)
[numr numc] = size(datain);
dataout = [];
for r = 1:numr
    values = datain(r,:);
    rr_variability = compute_variability(values);
    media = mean(datain(r,:));
    stand_dev = std(datain(r,:));
    varianza = var(datain(r,:));
    mediana = median(datain(r,:));
    kurt = kurtosis(datain(r,:));
    sk = skewness(datain(r,:));
    total_sum = sum(datain(r,:));
    range_m = max(datain(r,:)) - min(datain(r,:));
    rootms = rms(datain(r,:));
    prob = datain(r,:)/total_sum;
    entropy = -sum(prob.*log(prob));
    min_value = min(datain(r,:));
    max_value = max(datain(r,:));
    
    peaks = findpeaks(values);
    if(length(peaks)>1)
        mean_peaks = 0;
        dv = bsxfun(@minus,peaks,peaks');
        DD = sum(sum(triu(dv)));
        mean_peaks = (1/(length(peaks)^2))*DD;
    else
        mean_peaks = 0;
    end
    
    inter_q = iqr(values);
    %Fs = length(values);
    %powbp = bandpower(values,Fs,[0 Fs/2]);
    %
    % %[autocor,lags] = xcorr(values,60,'coeff');
    %mean_freq = meanfreq(values);
    %med_freq = medfreq(values);
    %
    %mom3 = moment(values,3);
    mom4 = moment(values,4);
    %
    mom5 = moment(values,5);
    %
    zz = abs(fft(values)/length(values));
    %fr_media1 = mean(zz);
    power_z = sqrt(sum(zz.^2));
    
    zz = zz(2:end);
    %power_z = sqrt(sum(zz.^2));
    %power_z = power_z(2:end);
    
    fr_media = mean(zz);
    %fr_stand_dev = std(zz);
    %fr_varianza = var(zz);
    fr_mediana = median(zz);
    % fr_kurt = kurtosis(zz);
    % fr_sk = skewness(zz);
    

    dataout = [dataout; rr_variability media min_value max_value mediana stand_dev varianza ...
        inter_q kurt sk total_sum range_m rootms entropy mom4 mom5 power_z fr_media fr_mediana];

    dataout(isnan(dataout))=0;
    dataout = real(dataout);
    dataout = dataout / norm(dataout);
    
end
 
 
 
 
end
 
 
 
function [dataout] = compute_variability(datain)
[numr numc] = size(datain);
rr_mean = mean(datain);
 
sdnn = sqrt((1/(length(datain)-1))*sum((datain/60-rr_mean).^2));
dd_datain = diff(datain/60);
rmssd =  sqrt((1/(length(datain)-1))*sum(dd_datain.^2));
pnn50 = sum(abs(dd_datain)>0.05)/length(datain);
 
dataout = [sdnn rmssd pnn50];
end