import math
import scipy.signal as sps
from scipy import signal
import pandas as pd
import pickle

G = 9.80665
N = 57.29

# Parameters for Butterworth filter
LOWCUT = 0.5
HIGHCUT = 24    # TOCHECK
FS = 50
ORDER = 5
BTYPE = 'bandpass'


def start_uniform(sensor, values, default_frequency, default_uom):
    temp = uniform_frequencies(sensor.get("frequency"), values, default_frequency)
    uniformed = uniform_uom(sensor.get("name"), sensor.get("uom"), temp, default_uom)
    return uniformed

# UNIFORM FREQUENCIES
def uniform_frequencies(current_frequency, values, new_frequency):
    if new_frequency != current_frequency:
        # uniform frequency
        if not current_frequency:
            '''
            if type(values[0] == dict):
                values_list = []
                for v in values:
                    values_list.append([x for x in v.values()])
                values = values_list
            '''

            # get frequency from timestamp
            # if frequency is empty timestamp cannot be empty
            # seconds ==> last timestamp - first timestamp / 1000000000
            seconds = math.ceil((values[-1].get('timestamp') - values[0].get('timestamp')) / 1000000000)
            # frequency ==> number of rows / seconds
            current_frequency = math.ceil(len(values) / seconds)

        x = [item.get('x') for item in values]
        y = [item.get('y') for item in values]
        z = [item.get('z') for item in values]
        df = pd.DataFrame({"x": x, "y": y, "z": z})
        number_of_samples = round(len(x) * float(new_frequency) / current_frequency)
        data_resample = sps.resample(df, number_of_samples)
        values = [[value[0], value[1], value[2]] for value in data_resample]
    # in both cases add fake timestamp
    result = []
    # todo modify timestamp
    for i, value in enumerate(values):
        if isinstance(value, dict):
            value = [value[key] for key in value.keys()]
        temp = value
        new_timestamp = i * (1 / new_frequency * 1000)
        temp.append(new_timestamp)
        result.append(temp)
    # return data in the following format: [x,y,z,timestamp]
    return result


# UNIFORM UOM
def uniform_uom(sensor, current_uom, values, default_uom):

    if sensor == 'accelerometer' and current_uom != default_uom:
        #g --> m/s^2
        if current_uom == 'g' and default_uom == 'm/s^2':
            new_acc = []
            for acc in values:
                new_acc.append([acc[0] * G, acc[1] * G, acc[2] * G, acc[3]])
            return remove_gravity(new_acc)
        
        # m/s^2 --> g
        if current_uom == 'm/s^2' and default_uom == 'g':
            new_acc = []
            for acc in values:
                new_acc.append([acc[0] / G, acc[1] / G, acc[2] / G, acc[3]])
            return remove_gravity(new_acc)
    
    if sensor == 'gyroscope' and current_uom != default_uom:
        #rad/s --> degree/s
        if current_uom == 'rad/s' and default_uom == 'degree/s':
            new_gyr = []
            for gyr in values:
                new_gyr.append([gyr[0] * N, gyr[1] * N, gyr[2] * N, gyr[3]])
            return remove_gravity(new_gyr)
        
        # degree/s --> rad/s
        if current_uom == 'degree/s' and default_uom == 'rad/s':
            new_gyr = []
            for gyr in values:
                new_gyr.append([gyr[0] / N, gyr[1] / N, gyr[2] / N, gyr[3]])
            return remove_gravity(new_gyr)

    return remove_gravity(values)


def compute_magnitude(accs):
    magnitude = []
    for acc in accs:
        if isinstance(acc, dict):
            x = acc.get("x")
            y = acc.get("y")
            z = acc.get("z")
        else:
            x = acc[0]
            y = acc[1]
            z = acc[2]
        temp = math.sqrt(x ** 2 + y ** 2 + z ** 2)
        magnitude.append(temp)
    return magnitude


# FILTER BUTTERWORTH
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq

    b, a = signal.butter(order, [low, high], btype=BTYPE)
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = signal.lfilter(b, a, data)
    return y

def remove_gravity(values):
  x = [ acc[0] for acc in values]
  y = [ acc[1] for acc in values]
  z = [ acc[2] for acc in values]

  filtered_x = butter_bandpass_filter(data = x, lowcut = LOWCUT, highcut = HIGHCUT, fs = FS)
  filtered_y = butter_bandpass_filter(data = y, lowcut = LOWCUT, highcut = HIGHCUT, fs = FS)
  filtered_z = butter_bandpass_filter(data = z, lowcut = LOWCUT, highcut = HIGHCUT, fs = FS)

  new_values = []
  for tmp_x, tmp_y, tmp_z in zip(filtered_x, filtered_y, filtered_z):
    new_values.append([tmp_x, tmp_y, tmp_z])

  return new_values