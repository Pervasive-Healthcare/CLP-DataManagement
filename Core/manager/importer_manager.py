from Core.MatLab.fe import extract_feature
from Core.RabbitMQ.message_sender import send_message
from Core.uniformer import start_uniform, compute_magnitude
from Utils.enums import KafkaKeys


# 4. Repository finish to save data in DB, request data with request_imported_data
def on_end_importing_default_response_received(msg):
    db_id = msg[KafkaKeys.DB_ID.value]
    request_imported_data_msg = {"type": "request_imported_data_default", "db_id": db_id}
    send_message('data_to_repo', request_imported_data_msg)
    print("--> Send request imported data (db_id: %s)" % db_id)


# 6. Uniform data received from repository with default config and extract feature from magnitude and resend it to repo
# for storing in a temp collection
def on_imported_data_received(msg):
    is_ok = True
    config = msg.get("config")
    uniformed_default = {
        "db_id": msg.get("db_id"),
        "type": "uniformed_default",
        "label": msg.get('label')
    }
    for sensor in msg.get("sensors"):
        sensor_type = sensor.get("name")
        if msg.get(sensor_type):
            if sensor_type == 'accelerometer':
                uniformed_default[sensor_type] = start_uniform(
                    sensor,
                    msg.get(sensor_type),
                    config.get("frequency"),
                    config.get("uom").get(sensor_type),
                )
                if not uniformed_default[sensor_type]:
                    print("Error uniforming sensor", sensor_type)
                    is_ok = False
                    break
        else:
            print("Sensor values for %s not found" % sensor_type)
            is_ok = False
            break
    if is_ok:
        acc_data = uniformed_default.get('accelerometer')
        magnitude = compute_magnitude(acc_data)
        fe = extract_feature(magnitude)

        uniformed_default['magnitude'] = magnitude
        uniformed_default['fe'] = fe.tolist()

        if uniformed_default.get('accelerometer'):
            del uniformed_default['accelerometer']

        if uniformed_default.get('gyroscope'):
            del uniformed_default['gyroscope']

        if uniformed_default.get('orientation'):
            del uniformed_default['orientation']

        send_message('data_to_repo', uniformed_default)


# 8 Send end uniforming default
def on_end_receiving_imported_data(data):
    data['type'] = 'end_uniforming_default'
    send_message('data_to_repo', data)






