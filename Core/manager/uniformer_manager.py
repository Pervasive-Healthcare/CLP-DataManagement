from Core.RabbitMQ.message_sender import send_message
from Core.uniformer import start_uniform
from Utils.enums import KafkaKeys

import yaml
import pickle
from pymongo import MongoClient
from Utils.enums import Config


# 2. Imported data are now with correct labels, send request to get all imported data
def on_end_to_uniform_labels(msg):
    db_id = msg[KafkaKeys.DB_ID.value]

    request_imported_data_msg = {"type": "request_imported_data", "db_id": db_id}
    send_message('req_to_repo', request_imported_data_msg)

    print("--> Send request imported data (db_id: %s)" % db_id)
    


# 4. Uniform data and send back to repository for storing in DB
def on_imported_labeled_data_received(msg):
    is_ok = True
    config = msg.get("config")
    uniformed = {
        "db_id": msg.get("db_id"),
        "type": "uniformed",
        "sensors": [],
        "config_id": config.get("id"),
        "label": msg.get('label'),
        "position": msg.get('position'),
        "device": msg.get('device'),
        "user": msg.get('user')
    }
    for sensor in msg.get("sensors"):
        sensor_type = sensor.get("name")
        if msg.get(sensor_type):
            uniformed["sensors"].append(
                {
                    "type": sensor_type,
                    "frequency": config.get("frequency"),
                    "uom": config.get("uom").get(sensor_type),
                }
            )
            uniformed[sensor_type] = start_uniform(
                sensor,
                msg.get(sensor_type),
                config.get("frequency"),
                config.get("uom").get(sensor_type),
            )
            if not uniformed[sensor_type]:
                print("Error uniforming sensor", sensor_type)
                is_ok = False
                break
        else:
            print("Sensor values for %s not found" % sensor_type)
            is_ok = False
            break
    if is_ok:
        send_message('data_to_repo', uniformed)
