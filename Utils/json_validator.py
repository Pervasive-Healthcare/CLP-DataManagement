import json
import os
from enum import Enum

from jsonschema import validate, ValidationError, SchemaError


class JsonSchema(Enum):
    QUERY_SCHEMA = "query_schema.json"
    DATA_SCHEMA = "data_schema.json"
    DBINFO_SCHEMA = "dbinfo_schema.json"
    CONFIG_SCHEMA = "config_schema.json"


schemas = {}
head, tail = os.path.split(os.path.dirname(__file__))

with open(os.path.join(head, "Utils", "config_schema.json")) as json_file:
    schemas[JsonSchema.CONFIG_SCHEMA] = json.load(json_file)
    json_file.close()


def validate_json(json_to_validate, json_schema, description):
    try:
        validate(instance=json_to_validate, schema=schemas[json_schema])
        return True
    except ValidationError:
        print("%s - ValidationError" % description)
        return False
    except SchemaError:
        print("%s - SchemaError" % description)
        return False



