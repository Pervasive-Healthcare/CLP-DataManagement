from enum import Enum


class Sensors(Enum):
    ACCELEROMETER = "accelerometer"
    LINEAR_ACCELEROMETER = "linear accelerometer"
    GYROSCOPE = "gyroscope"
    MAGNETOMETER = "magnetometer"
    ORIENTATION = "orientation"


class Uom(Enum):
    METERS_SQUARE_SECONDS = "m/s^2"
    RAD_SEC = "rad/sec"
    MICRO_TESLA = "uT"


class Config(Enum):
    RABBIT_MQ_CONNECTION = "rabbitmq_connection"
    MONGO_CONNECTION = "mongodb_connection"
    KAFKA_CONNECTION = "kafka_connection"
    DB_NAME = "db_name"
    HOST = "host"
    PORT = "port"


class CollectionsName(Enum):
    IMPORTED = "Imported"
    CONFIG = "Config"
    UNIFORMED = "Uniformed"


class KafkaTopic(Enum):
    REQUEST = "request"
    RESPONSE = "response"
    DATA = "data"
    UNIFORMED = "uniformed"
    LABELS = "labelss"


class KafkaKeys(Enum):
    CONFIG = "Config"
    IMPORTED = "Imported"
    ID = "Id"
    NEW_ID = "New_Id"
    DB_NAME = "db_name"
    DB_ID = "db_id"
    DELETE = "Delete"
